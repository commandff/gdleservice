package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

var (
	elog   debug.Log
	ewInfo *eWInfo
)

type eWInfo struct {
	io.Writer
}

func (e eWInfo) Write(p []byte) (int, error) {
	return len(p), elog.Info(1000, string(p[:len(p)-1]))
}

type myservice struct{}

func main() {

	// if we are running in a windows service, go do that.
	inService, err := svc.IsWindowsService()
	if err != nil {
		elog.Error(1, fmt.Sprintf("failed to determine if we are running in service: %v\n", err))
		os.Exit(2)
	}
	if inService {
		elog, err = eventlog.Open("GDLEnhanced")
		if err != nil {
			elog.Error(1, fmt.Sprintf("eventlog.Open(\"GDLEnhanced\") error: %v\n", err))
			os.Exit(2)
		}
		defer elog.Close()
		err = svc.Run("GDLEnhanced", &myservice{})
		if err != nil {
			elog.Error(1, fmt.Sprintf("GDLEnhanced service failed: %v", err))
			os.Exit(2)
		}
		os.Exit(0)
	}

	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "usage: %s <gdle executable> [options]\n\t\tex: \"c:\\gdle\\GDLEnhanced.exe\" -v 1 -c server.cfg -s\n", filepath.Base(os.Args[0]))
		os.Exit(2)
	}
	if _, err := os.Open("\\\\.\\PHYSICALDRIVE0"); err != nil {
		fmt.Fprintf(os.Stderr, "This program requires account elevation\n")
		os.Exit(2)
	}
	m, err := mgr.Connect()
	if err != nil {
		fmt.Fprintf(os.Stderr, "mgr.Connect() error: %v\n", err)
		os.Exit(2)
	}
	defer m.Disconnect()
	s, err := m.OpenService("GDLEnhanced")
	if err == nil {
		defer s.Close()
		fmt.Fprintf(os.Stderr, "GDLEnhanced Service is already installed\n")
		os.Exit(2)
	}
	fmt.Fprintf(os.Stderr, "Installing GDLEnhanced Service...\n")
	s, err = m.CreateService("GDLEnhanced", os.Args[0], mgr.Config{
		ServiceType:      windows.SERVICE_WIN32_OWN_PROCESS, // ServiceType      uint32 1=Kernel Mode, 2=Kernel Mode w/FS, 4=Network, 16=Standalone, 32=Shared
		StartType:        windows.SERVICE_AUTO_START,        // StartType        uint32 0 = Boot, 1 = System, 2 = Autostart, 3 = Manual Start, 4 = Disabled
		ErrorControl:     windows.SERVICE_ERROR_NORMAL,      // ErrorControl     uint32 0=Ignore, 1=Normal, 2=Severe, 3=Critical
		BinaryPathName:   strings.Join(os.Args, " "),        // BinaryPathName   string // fully qualified path to the service binary file, can also include arguments for an auto-start service
		LoadOrderGroup:   "",                                // LoadOrderGroup   string
		TagId:            0,                                 // TagId            uint32
		Dependencies:     []string{"MariaDB"},               // Dependencies     []string
		ServiceStartName: "",                                // ServiceStartName string // name of the account under which the service should run
		DisplayName:      "GDLEnhanced",                     // DisplayName      string
		Password:         "",                                // Password         string
		Description:      "An AC Emulator",                  // Description      string
		SidType:          windows.SERVICE_SID_TYPE_NONE,     // SidType          uint32 // one of SERVICE_SID_TYPE, the type of sid to use for the service
		DelayedAutoStart: false,                             // DelayedAutoStart bool   // the service is started after other auto-start services are started plus a short delay

	}, os.Args[1:]...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "m.CreateService() error: %v\n", err)
		os.Exit(2)
	}
	defer s.Close()
	err = eventlog.InstallAsEventCreate("GDLEnhanced", eventlog.Error|eventlog.Warning|eventlog.Info)
	if err != nil {
		// s.Delete()
		fmt.Fprintf(os.Stderr, "SetupEventLogSource() failed %v\n", err)
		// os.Exit(2)
	}
	fmt.Fprintf(os.Stderr, "GDLEnhanced service installed.\n")

}
func (m *myservice) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	changes <- svc.Status{State: svc.StartPending}

	ewInfo = &eWInfo{}
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile | log.Lmsgprefix | log.LUTC)
	log.SetOutput(ewInfo)
	cmd := exec.Command(os.Args[1], os.Args[2:]...)
	cmd.Dir = filepath.Dir(os.Args[1])
	// gdlestdout, _ := cmd.StdoutPipe()
	err := cmd.Start()
	if err != nil {
		elog.Error(1, fmt.Sprintf("Start Failed: %v", err))
		changes <- svc.Status{State: svc.StopPending}
		os.Exit(2)
	}
	kevorkian := make(chan int)
	go func(kevorkian chan int) {
		cmd.Wait()
		kevorkian <- 1
	}(kevorkian)
	changes <- svc.Status{State: svc.Running, Accepts: svc.AcceptStop | svc.AcceptShutdown}
	for {
		select {
		case <-kevorkian:
			ec := cmd.ProcessState.ExitCode()
			elog.Error(1, fmt.Sprintf("GDLE Processess terminated (exit code %d).", ec))
			return ec == 0, 0
		case comd := <-r:
			switch comd.Cmd {
			case svc.Interrogate:
				changes <- comd.CurrentStatus
				time.Sleep(100 * time.Millisecond)
				changes <- comd.CurrentStatus
			case svc.Stop, svc.Shutdown:
				changes <- svc.Status{State: svc.StopPending}
				elog.Error(1, "Shutdown Received")
				err = cmd.Process.Signal(os.Kill)
				if err != nil {
					elog.Error(1, fmt.Sprintf("cmd.Process.Signal(os.Kill) error: %v", err))
				}
			default:
				elog.Error(1, fmt.Sprintf("unexpected control request #%d", comd))
			}
		}
	}
}
