# what?
GDLEService is a Windows Service wrapper for GDLEnhanced v1.38 release.
# Usage
## Install:
- Place gdleservice.exe somewhere accessible, like your gdle server folder. `gdleservice.exe` is the installer AND service binary.
- `gdleservice.exe <gdle executable> [options]`
- `c:\gdle\gdleservice.exe "c:\gdle\gdlenhanced.exe" -v 1 -c server.cfg -s`: install GDLEnhanced service
- alternatively, install_service.reg and uninstall_service.reg are provided, with some safe defaults.
## Control:
- from Windows `services.msc`, Start/Stop/etc should work normally, on the `GDLEnhanced` service.
- `"C:\\WINDOWS\\SYSTEM32\\SC.EXE" start GDLEnhanced`: Start GDLEnhanced Service
- `"C:\\WINDOWS\\SYSTEM32\\SC.EXE" stop GDLEnhanced`: Stop GDLEnhanced Service
- `"C:\\WINDOWS\\SYSTEM32\\SC.EXE" delete GDLEnhanced`: Delete GDLEnhanced Service
